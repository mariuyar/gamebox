#define _POSIX_C_SOURCE 200112L
#include "utils.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <termios.h> // для отключения ожидания нажатия enter

#include "math.h"
#include "lcd.h"

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"

void draw_rect( LCD * lcd, int x, int y, int w, int h, Color color )
{
    for ( int i = y; i < y+h; i++)
        for (int j = x; j < x+w; j++)
            lcd_set_pixel( lcd, j, i, color_to_lcdcolor( color ) );
}

void draw_circle( LCD * lcd, int cx, int cy, int radius, Color color, int fill )
{
    unsigned short col = color_to_lcdcolor( color );
    for (int R = (fill == 1) ? 0 : radius; R <= radius; R++)
    {
        for ( double i = 0; i < 360; i+=0.5 )
        {
            double x = R * cos(i * 3.14159 / 180.0);
            double y = R * sin(i * 3.14159 / 180.0);
            lcd_set_pixel( lcd, cx + x, cy + y, col );
        }
    }
}

void ledline_show( LCD * lcd, uint32_t value )
{
    *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_LINE_o) = value;
}

void ledline_show_battle_score( LCD * lcd, int len1, int len2 )
{
    int total_len = len1 + len2;
    int a = (len1<<5)*1.0/total_len;
    int b = (len2<<5)*1.0/total_len;
    uint32_t val = UINT32_MAX;
    val &= ~(1<<(31-a));
    val &= ~(1<<( 31-a == b ? b-1 : b ));
    *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_LINE_o) = val;
}

void ledline_show_snake_len( LCD * lcd, int snake_len, int maxlen )
{
    int len = ((snake_len << 5)*1.0/maxlen)+1;
    uint32_t val = 0;
    for (uint32_t i = 1<<31; i > 1<<(31-len); i = i >> 1)
        val |= i;
    *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_LINE_o) = val;
}

void led_blink( LCD * lcd, uint8_t r, uint8_t g, uint8_t b, int delay_ms, int cnt )
{
    uint32_t value = b;
    value |= g << 8;
    value |= r << 16;
    *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_RGB1_o) = 0u;
    *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_RGB2_o) = 0u;

    struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = delay_ms * 1000000 }; // 1ms = 1000000ns
    for (int i = 0; i < cnt; i++)
    {
        *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_RGB1_o) = value;
        *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_RGB2_o) = value;
        clock_nanosleep( CLOCK_MONOTONIC, 0, &loop_delay, NULL );  // delay 1s
        *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_RGB1_o) = 0u;
        *(volatile uint32_t*)(lcd->mem_base + SPILED_REG_LED_RGB2_o) = 0u;
        clock_nanosleep( CLOCK_MONOTONIC, 0, &loop_delay, NULL ); // delay 1s
    }
}
