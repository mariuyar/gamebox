#ifndef __GAME_H__
#define __GAME_H__

#include "lcd.h"
#include "font.h"
#include "map.h"
#include "snake.h"

typedef struct GAME
{
    MAP * m_map;
    LCD * m_lcd;
    FONT * m_font;
    int m_snakes_cnt;
    // если змей будет больше 9 нужно изменить функцию game_print_score !
    int m_max_snakes_cnt;
    SNAKE * m_snakes;
}GAME;

void game_init( GAME * game, LCD * lcd, FONT * font );
void game_end( GAME * game );
void game_start( GAME * game, int game_mode );
void game_add_snake( GAME * game, 
                     int control, 
                     Color color, 
                     double speed );
void game_draw( GAME * game );
void game_print_score( GAME * game );
void game_blink_single( GAME * game );
void game_blink_many( GAME * game );

#endif//__GAME_H__
