#ifndef __SNAKE_H__
#define __SNAKE_H__

#include "color.h"
#include "lcd.h"
#include "map.h"
#include "utils.h"

enum SNAKE_CONTROL{ MANUAL, AUTOPILOT };

typedef struct SNAKE
{
    MAP * m_map;
    int m_control; // как будет управляться 
    Color m_color;
    int m_cellsize;  // size of quadr w, h
    double m_speed; // скорость змеи в клетках за единицу времени
    COORDS m_direction;
    int m_len;
    int m_resevate;
    COORDS m_coords; // координаты головы на карте
    COORDS m_pix_coords; // координаты головы в пикселях
    COORDS * m_body; // координаты всего тела начиная с головы
    COORDS m_last_dir_choose;
}SNAKE;

void snake_init( SNAKE * snake, 
                 int control,
                 MAP * map, 
                 Color color, 
                 int cellsize, 
                 double speed,
                 int x, 
                 int y );

void snake_draw( SNAKE * snake, LCD * lcd );
void snake_update_direction( SNAKE * snake, COORDS direction );
void snake_move( SNAKE * snake );
// тело вытягивается начиная с головы
void snake_update_body( SNAKE * snake );
// удлиняет змею на 1 блок с хвоста
void snake_eat_apple( SNAKE * snake );
// проверяет если первая змея укусила другую змею 
int snake_eat_snake( SNAKE * snake1, SNAKE * snake2 );
// проверяет если змея укусила саму себя
int snake_eat_herself( SNAKE * snake );
// проверяет если змея врезелась в стену
int snake_eat_wall( SNAKE * snake );


#endif//__SNAKE_H__