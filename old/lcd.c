#include "lcd.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"


// инициализация дисплея
int lcd_init( LCD * lcd )
{
    lcd->width = SCREEN_WIDTH;
    lcd->height = SCREEN_HEIGHT;
    lcd->buffer = NULL;
    lcd->mem_base = NULL;
    lcd->parlcd_mem_base = NULL;

    lcd->buffer = (unsigned short *)malloc(lcd->width*lcd->height*2);
        /*
    * Setup memory mapping which provides access to the peripheral
    * registers region of RGB LEDs, knobs and line of yellow LEDs.
    */
    lcd->mem_base = (unsigned char *)map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);

    /* If mapping fails exit with error code */
    if ( lcd->mem_base == NULL )
        return 0;

    lcd->parlcd_mem_base = (unsigned char *)map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);

    if ( lcd->parlcd_mem_base == NULL )
        return 0;

    parlcd_hx8357_init( lcd->parlcd_mem_base );

    return 1;
}

void lcd_clear( LCD * lcd )
{
    lcd_background( lcd, 0u );    
}

// Вывод буффера дисплея на экран
void lcd_show( LCD * lcd )
{
    parlcd_write_cmd( lcd->parlcd_mem_base, 0x2c );
    for ( int i = 0; i < lcd->width*lcd->height; i++ )
        parlcd_write_data( lcd->parlcd_mem_base, lcd->buffer[i] );
}

// заполняет экран сплошным цветом
void lcd_background( LCD * lcd, unsigned short color )
{
    for ( int i = 0; i < lcd->width*lcd->height; i++ )
        lcd->buffer[i] = color;
}

// Устанавливает в буффере цвет одного пикселя
void lcd_set_pixel( LCD * lcd, int x, int y, unsigned short color) 
{
    if (x>=0 && x< lcd->width && y>=0 && y<lcd->height) 
        lcd->buffer[x + lcd->width*y] = color;
}