#ifndef __LCD_H__
#define __LCD_H__

#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 320

typedef struct LCD
{
    int height;
    int width;
    unsigned short * buffer; // буффер дисплея
    unsigned char * mem_base;
    unsigned char * parlcd_mem_base;
}LCD;

// инициализация дисплея
int lcd_init( LCD * lcd );

// Вывод буффера дисплея на экран
void lcd_show( LCD * lcd );

// очищает экран
void lcd_clear( LCD * lcd );

// заполняет экран сплошным цветом
void lcd_background( LCD * lcd, unsigned short color );

// Устанавливает в буффере цвет одного пикселя
void lcd_set_pixel( LCD * lcd, int x, int y, unsigned short color);

#endif//__LCD_H__