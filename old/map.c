#include "map.h"
#include "stdlib.h"
#include "color.h"
#include "utils.h"
#include "stdio.h"

void map_init( MAP * map, int screen_width, int screen_height, int cell_size, int walls )
{
    map->cell_size = cell_size;
    map->m_width =  screen_width/cell_size;
    map->m_height = screen_height/cell_size;
    map->m_data = (unsigned char*)malloc( map->m_width * map->m_height * sizeof(unsigned char*) );
    // заполнение карты пустотой
    for (int i = 0; i < map->m_width*map->m_height; i++)
        map->m_data[i] = EMPTY;
    if( walls == 1 )
        map_create_walls( map );
    map->create_apple = 0;
}

void map_create_walls( MAP * map )
{
    // создание стены сверху снизу
    for (int i = 0; i < map->m_width; i++)
    {
        map->m_data[i] = WALL;
        map->m_data[(map->m_height-1)*map->m_width+i] = WALL;
    }
    // создание стены слева справа
    for (int i = 0; i < map->m_height; i++)
    {
        map->m_data[i*map->m_width] = WALL;
        map->m_data[i*map->m_width+(map->m_width-1)] = WALL;
    }
}

void map_draw( MAP * map, LCD * lcd )
{
    for (int i = 0; i < map->m_height; i++)
    {
        for (int j = 0; j < map->m_width; j++)
        {
            int pos = i*map->m_width + j;
            if( map->m_data[pos] == WALL )
                draw_rect( lcd, j*map->cell_size, i*map->cell_size, map->cell_size, map->cell_size, create_color( 20, 20, 20 ) );
            else if( map->m_data[pos] == APPLE )
                draw_rect( lcd, j*map->cell_size, i*map->cell_size, map->cell_size, map->cell_size, create_color( 0, 63, 0 ) );
        }
    }
}

void map_generate_apple( MAP * map )
{
    int x = 1+rand()%(map->m_width-1);
    int y = 1+rand()%(map->m_height-1);
    while ( map->m_data[y*map->m_width+x] == WALL )
    {
        x = rand()%map->m_width;
        y = rand()%map->m_height;
    }
    map->m_data[y*map->m_width+x] = APPLE;
    map->create_apple = 1;
    map->apple_coords.x = x;
    map->apple_coords.y = y;
    //printf("create apple --> x: %d / y: %d\n", x , y);
}

int map_is_apple( MAP * map, int x, int y )
{
    int pos = y*map->m_width + x;
    return map->m_data[pos] == APPLE;
}

int map_is_wall( MAP * map, int x, int y )
{
    int pos = y*map->m_width + x;
    return map->m_data[pos] == WALL;
}

void map_remove_apple( MAP * map, int x, int y )
{
    int pos = y*map->m_width + x;
    if( map->m_data[pos] != APPLE )
    {
        printf("There is no apple!\n");
        return;
    }
    map->m_data[pos] = EMPTY;
    map->create_apple = 0;
}
