#define _POSIX_C_SOURCE 200112L

#include "game.h"
#include "menu.h"
#include "font.h"
#include "color.h"
#include "lcd.h"
#include "utils.h"
#include "map.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"
#include "termios.h"
#include "time.h"
#include <unistd.h>
#include "math.h"
#include "snake_controller.h"

#define CELLSIZE 20

void game_init( GAME * game, LCD * lcd, FONT * font )
{
    game->m_lcd = lcd;
    game->m_font = font;
    MAP * map = (MAP*)malloc(sizeof(MAP));
    map_init( map, lcd->width, lcd->height, CELLSIZE, NOWALLS );
    game->m_map = map;
    printf("map size: w = %d, h = %d\n", map->m_width, map->m_height);
    game->m_snakes_cnt = 0;
    game->m_max_snakes_cnt = 5;
    game->m_snakes = (SNAKE*)malloc(game->m_max_snakes_cnt*sizeof(SNAKE));
}

void game_end( GAME * game )
{
    free(game->m_map);
    free(game->m_snakes);
}

void game_add_snake( GAME * game, 
                     int control, 
                     Color color, 
                     double speed )
{
    if( game->m_snakes_cnt > game->m_max_snakes_cnt )
    {
        printf("Max snakes count!\n");
        return;
    }

    SNAKE new_snake;
    int x = 1 + rand()%(game->m_map->m_width-1);
    int y = 1 + rand()%(game->m_map->m_height-1);
    snake_init( &new_snake, control, game->m_map, color, CELLSIZE, speed, x, y ); 
    printf("Add snake: control: %d, coords x: %d y: %d\n", control, x, y);
    game->m_snakes[game->m_snakes_cnt++] = new_snake;
}

void game_move( GAME * game );
int game_control( GAME * game );
void game_eat_apple( GAME * game );

void game_start( GAME * game, int game_mode )
{
    int max_snakes_len = game->m_map->m_height*game->m_map->m_width;

    int quit = 0;
    while ( ! quit )
    {
        // создаем новое яблоко
        if( game->m_map->create_apple == 0 )
            map_generate_apple( game->m_map );
        // обновление направления и движение змей
        game_move( game );
        // проверка на столкновения
        quit = game_control( game );
        // проверка на съедения яблока
        game_eat_apple( game );
        // рисование карты и змей
        game_draw( game );    
        // вывод статуса длины змейки
        if( game_mode == MODE1 )
            ledline_show_snake_len( game->m_lcd, game->m_snakes[0].m_len, max_snakes_len );
        else if( game_mode == MODE2 && game->m_snakes_cnt == 2 )
            ledline_show_battle_score( game->m_lcd, game->m_snakes[0].m_len, game->m_snakes[1].m_len );
    }
    game_print_score( game );
    // delay 2 sec
    struct timespec loop_delay = {.tv_sec = 2, .tv_nsec = 0 }; // 1ms = 1000000ns
    clock_nanosleep( CLOCK_MONOTONIC, 0, &loop_delay, NULL );
}

void game_move( GAME * game )
{
    for (int i = 0; i < game->m_snakes_cnt; i++)
    {
        SNAKE * snake = &game->m_snakes[i];
        if( snake->m_control == MANUAL )
            controll_update_direction_manual( game, snake );
        else if( snake->m_control == AUTOPILOT )
            controll_update_direction_auto( game, snake );
        snake_move( snake );
    }        
}

int game_control( GAME * game )
{
    int res = 0;
    // проверка на врезание в стены\самого в себя\в другую змею
    for (int i = 0; i < game->m_snakes_cnt; i++)
    {
        if( snake_eat_herself( &game->m_snakes[i] ) )
        {
            printf( "snake #%d eat herself!\n", i );
            res = 1;
        }
        for (int j = 0; j < game->m_snakes_cnt; j++)
        {
            if( i != j )
            {
                if( snake_eat_snake( &game->m_snakes[i], &game->m_snakes[j] ) )
                {
                    printf( "snake #%d eat snake #%d !\n", i, j );
                    res = 1;
                }
            }
        }
        if( snake_eat_wall( &game->m_snakes[i] ) )
        {
            printf("snake #%d eat wall!\n", i);
            res = 1;
        }
    }
    return res;
}

void game_eat_apple( GAME * game )
{
    // проверика если кто-то скушал яблоко
    for (int i = 0; i < game->m_snakes_cnt; i++)
    {
        int x = (int)game->m_snakes[i].m_coords.x;
        int y = (int)game->m_snakes[i].m_coords.y;
        if( map_is_apple( game->m_map, x, y ) )
        {
            map_remove_apple( game->m_map, x, y );
            snake_eat_apple( &game->m_snakes[i] );
        }
    }
}

void game_draw( GAME * game )
{
    lcd_clear( game->m_lcd );
    map_draw( game->m_map, game->m_lcd );
    for (int i = 0; i < game->m_snakes_cnt; i++)
        snake_draw( &game->m_snakes[i], game->m_lcd );
    lcd_show( game->m_lcd );
}

void game_print_score( GAME * game )
{
    lcd_clear( game->m_lcd );
    for (size_t i = 0; i < game->m_snakes_cnt; i++)
    {
        char * text = (char*)malloc(30*sizeof(char));
        sprintf( text, "%d snake len: %d%c", 1+i, game->m_snakes[i].m_len, '\0' );
        printf("%s\n", text);
        font_draw_text( game->m_font, 
                        game->m_lcd, 
                        text, strlen(text), 
                        game->m_lcd->width/2, 
                        (i+1)*game->m_lcd->height/(game->m_snakes_cnt+1), 
                        game->m_snakes[i].m_color,
                        CENTER );
        free(text);
    }
    lcd_show( game->m_lcd );
    // если змейка одна то мигает зеленым в случае если достигнута максимальная длина, инача красным
    if( game->m_snakes_cnt == 1 )
        game_blink_single( game );
    // если змеек много то LED1,2 мигает цветом победителя
    else if( game->m_snakes_cnt > 1 )
        game_blink_many( game );
}

// при победе мигает зеленым, иначе - красным
void game_blink_single( GAME * game )
{
    int max_snakes_len = game->m_map->m_height*game->m_map->m_width;
    if( game->m_snakes[0].m_len == max_snakes_len )
        led_blink( game->m_lcd, 0x00, 0xff, 0x00, 100, 4 );
    else
        led_blink( game->m_lcd, 0xff, 0x00, 0x00, 100, 4 );
}

// мигает цветом пободителя
void game_blink_many( GAME * game )
{
    int max_len = 0;
    Color col;
    for (int i = 0; i < game->m_snakes_cnt; i++)
        if( game->m_snakes[i].m_len > max_len )
        {
            max_len = game->m_snakes[i].m_len;
            col = game->m_snakes[i].m_color;
        }
    if( max_len > 0 )
        led_blink( game->m_lcd, col.r, col.g, col.b, 100, 8 );
}