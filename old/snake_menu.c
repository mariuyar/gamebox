#include "menu.h"
#include "color.h"
#include "lcd.h"
#include "font.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "termios.h"
#include "unistd.h"

void menu_init( MENU * menu )
{
    menu->m_state = NONE;
    menu->m_mode = MODE1;
    menu->m_game_speed = SPEED2;
    menu->m_game_color = BLUE;
}

void menu_draw( MENU * menu, LCD * lcd, FONT * font )
{
    // количество элементов в меню для вовода на экран
    // влияет на Х координаты
    int menu_items = 2;
    if( menu->m_mode == MODE1 )
        menu_items += 2;
    // запомнить начальный размер шрифта
    int init_font_size = font->size;
    // невыделенный текст
    Color def_text_color = create_color( 0, 0, 31 );
    // выделенный текст
    Color select_text_color = create_color( 20, 0, 31 );

    lcd_background( lcd, rgb_to_lcdcolor( 0, 0, 0 ) );
    
    if( menu->m_state == START ) font->size += 2;
    font_draw_text( font,
                    lcd, 
                    "START", 
                    5, // text len
                    lcd->width/2, 
                    1*lcd->height/(menu_items+1), 
                    menu->m_state == START ? select_text_color : def_text_color, 
                    CENTER );
    font->size = init_font_size;

    char * text = (char*)malloc(20*sizeof(char));
    if( menu->m_mode == MODE1 )
        strcpy( text, "< Single >\0" );
    else
        strcpy( text, "< Demo >\0" );
    
    if( menu->m_state == MODE ) font->size += 2;
    font_draw_text( font,
                    lcd, 
                    text, 
                    strlen(text), 
                    lcd->width/2, 
                    2*lcd->height/(menu_items+1),
                    menu->m_state == MODE ? select_text_color : def_text_color, 
                    CENTER );
    font->size = init_font_size;

    if( menu->m_mode == MODE1 )// if Single mode selected
    {
        if( menu->m_game_speed == SPEED1 )
            strcpy( text, "< Low >\0" );
        else if( menu->m_game_speed == SPEED2 )
            strcpy( text, "< Medium >\0" );
        else
            strcpy( text, "< Fast >\0" );

        if( menu->m_state == SPEED ) font->size += 2;
        font_draw_text( font,
                        lcd, 
                        text, 
                        strlen(text), 
                        lcd->width/2, 
                        3*lcd->height/(menu_items+1),
                        menu->m_state == SPEED ? select_text_color : def_text_color, 
                        CENTER );
        font->size = init_font_size;

        if( menu->m_game_color == RED )
            strcpy( text, "< Red >\0" );
        else if( menu->m_game_color == GREEN )
            strcpy( text, "< Green >\0" );
        else
            strcpy( text, "< Blue >\0" );

        if( menu->m_state == COLOR ) font->size += 2;
        font_draw_text( font,
                        lcd,
                        text,
                        strlen(text),
                        lcd->width/2,
                        4*lcd->height/(menu_items+1),
                        menu->m_state == COLOR ? select_text_color : def_text_color,
                        CENTER );
        font->size = init_font_size;
    }

    free(text);
}

void menu_control_state( MENU * menu )
{
    if( menu->m_state > COLOR )
        menu->m_state = START;
    else if( menu->m_state < START )
        menu->m_state = COLOR;    
}

void menu_control_mode( MENU * menu )
{
    if( menu->m_mode < MODE1 )
        menu->m_mode = MODE2;
    else if( menu->m_mode > MODE2 )
        menu->m_mode = MODE1;
}

void menu_control_game_speed( MENU * menu )
{
    if( menu->m_game_speed < SPEED1 )
        menu->m_game_speed = SPEED3;
    else if( menu->m_game_speed > SPEED3 )
        menu->m_game_speed = SPEED1;
}

void menu_control_game_color( MENU * menu )
{
    if( menu->m_game_color < RED )
        menu->m_game_color = BLUE;
    else if( menu->m_game_color > BLUE )
        menu->m_game_color = RED;
}

void menu_control_values( MENU * menu )
{
    menu_control_state( menu );
    menu_control_mode( menu );
    menu_control_game_speed( menu );
    menu_control_game_color( menu );
}

int menu_event( MENU * menu )
{
    char ch;
    int r = read( 0, &ch, 1 );
    if( r != 1 ) return 0;
    printf( "sym: %c / %d\n", ch, ch );

    if( ch == 10 ) // enter
        return 1;
    else if( ch == 's' )
        menu->m_state++;
    else if( ch == 'w' )
        menu->m_state--;
    else if( ch == 'a' )
    {
        if( menu->m_state == MODE )
            menu->m_mode--;
        else if( menu->m_state == SPEED )
            menu->m_game_speed--;
        else if( menu->m_state == COLOR )
            menu->m_game_color--;
    }
    else if( ch == 'd' )
    {
        if ( menu->m_state == MODE )
            menu->m_mode++;
        else if( menu->m_state == SPEED )
            menu->m_game_speed++;
        else if( menu->m_state == COLOR )
            menu->m_game_color++;
    }
    menu_control_values( menu );
    return 0;
}