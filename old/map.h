#ifndef __MAP_H__
#define __MAP_H__

#include "lcd.h"
#include "utils.h"

#define NOWALLS 0
#define WITHWALLS 1

enum MAP_OBJECTS{ EMPTY, APPLE, WALL };

typedef struct MAP
{
    int m_width;
    int m_height;
    int cell_size;
    unsigned char * m_data;
    int create_apple;
    COORDS apple_coords;
}MAP;

void map_init( MAP * map, int screen_width, int screen_height, int cell_size, int walls );
void map_create_walls( MAP * map );
/*!
    walls - white
    apple - green
*/
void map_draw( MAP * map, LCD * lcd );
void map_generate_apple( MAP * map );
int map_is_apple( MAP * map, int x, int y );
int map_is_wall( MAP * map, int x, int y );
void map_remove_apple( MAP * map, int x, int y );
#endif//__MAP_H__   