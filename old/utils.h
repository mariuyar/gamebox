#ifndef __UTILS_H__
#define __UTILS_H__

#include "color.h"
#include "lcd.h"
#include "stdint.h"

void draw_rect( LCD * lcd, int x, int y, int w, int h, Color color );
void draw_circle( LCD * lcd, int cx, int cy, int radius, Color color, int fill );
void ledline_show( LCD * lcd, uint32_t value );
void ledline_show_battle_score( LCD * lcd, int len1, int len2 );
void ledline_show_snake_len( LCD * lcd, int snake_len, int maxlen );

/*
    R - 23..16 | 8bit
    G - 15..8  | 8bit
    B - 7..0   | 8bit
*/
void led_blink( LCD * lcd, uint8_t r, uint8_t g, uint8_t b, int delay_ms, int cnt );

typedef struct COORDS
{
    double x;
    double y;
}COORDS;

#endif//__UTILS_H__