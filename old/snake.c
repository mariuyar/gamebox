#include "snake.h"
#include "utils.h"
#include "stdlib.h"
#include "stdio.h"
#include "termios.h"
#include "unistd.h"

void snake_init( SNAKE * snake, 
                 int control,
                 MAP * map, 
                 Color color, 
                 int cellsize, 
                 double speed,
                 int x, 
                 int y )
{
    snake->m_map = map;
    snake->m_control = control;
    snake->m_color = color;
    snake->m_speed = speed;
    snake->m_cellsize = cellsize;
    snake->m_speed = speed;
    snake->m_direction.x = 0; // изначально змейка стоит на месте
    snake->m_direction.y = 0;
    snake->m_len = 1;
    snake->m_resevate = 10;
    snake->m_body = (COORDS*)malloc(snake->m_resevate*sizeof(COORDS));
    snake->m_coords.x = x;  // голова
    snake->m_coords.y = y;
    snake->m_pix_coords.x = x*cellsize;
    snake->m_pix_coords.y = y*cellsize;
    snake->m_body[0].x = x; // змея состоит из 1 блока
    snake->m_body[0].y = y;
}    

void snake_draw( SNAKE * snake, LCD * lcd )
{
    draw_rect( lcd, snake->m_coords.x * snake->m_cellsize, // x
                   snake->m_coords.y * snake->m_cellsize, // y
                   snake->m_cellsize, // w
                   snake->m_cellsize, // h
                   create_color( 255, 255, 255 ) );
    for (int i = 1; i < snake->m_len; i++)
        draw_rect( lcd,
                   snake->m_body[i].x*snake->m_cellsize, // x
                   snake->m_body[i].y*snake->m_cellsize, // y
                   snake->m_cellsize, // w
                   snake->m_cellsize, // h
                   snake->m_color );
}

void snake_update_direction( SNAKE * snake, COORDS direction )
{
    if( snake->m_coords.x == snake->m_last_dir_choose.x
        && snake->m_coords.y == snake->m_last_dir_choose.y ) 
        return;

    COORDS next_direction = direction;

    // нельзя идти в обратном направлении
    if( next_direction.x == (int)snake->m_direction.x * -1
        &&  next_direction.y == (int)snake->m_direction.y * -1 )
        next_direction = snake->m_direction;
    
    if( snake->m_direction.x != next_direction.x && snake->m_direction.y != next_direction.y )
    {
        snake->m_last_dir_choose = snake->m_coords;
        //printf( "new_dir= x: %d y: %d\n", (int)next_direction.x, (int)next_direction.y );
    }

    snake->m_direction = next_direction;

    // отцентрировать по осям при изменении направления
    if(next_direction.x != 0)
        snake->m_pix_coords.y = snake->m_coords.y * snake->m_cellsize;
    if(next_direction.y != 0)
        snake->m_pix_coords.x = snake->m_coords.x * snake->m_cellsize;
}

void snake_move( SNAKE * snake )
{
    snake->m_pix_coords.x += (int)snake->m_direction.x * snake->m_speed * snake->m_cellsize;
    snake->m_pix_coords.y += (int)snake->m_direction.y * snake->m_speed * snake->m_cellsize;
    
    snake->m_coords.x = (int)(snake->m_pix_coords.x / snake->m_cellsize);
    snake->m_coords.y = (int)(snake->m_pix_coords.y / snake->m_cellsize);

    int beyong_the_screen = 0;
    if( snake->m_coords.x < 0 ) 
    {
        snake->m_coords.x = snake->m_map->m_width-1;
        beyong_the_screen = 1;
    }
    else if( snake->m_coords.y < 0 ) 
    {
        snake->m_coords.y = snake->m_map->m_height-1;
        beyong_the_screen = 1;
    }
    else if( snake->m_coords.x >= snake->m_map->m_width )  
    {
        snake->m_coords.x = 0;
        beyong_the_screen = 1;
    }
    else if( snake->m_coords.y >= snake->m_map->m_height ) 
    {
        snake->m_coords.y = 0;
        beyong_the_screen = 1;
    }
    if( beyong_the_screen == 1 )
    {
        snake->m_pix_coords.x = (int)snake->m_coords.x * snake->m_cellsize;
        snake->m_pix_coords.y = (int)snake->m_coords.y * snake->m_cellsize;
    }
    snake_update_body( snake );
}

void snake_update_body( SNAKE * snake )
{
    if( snake->m_coords.x == snake->m_body[0].x
        && snake->m_coords.y == snake->m_body[0].y )
            return;
    for (int i = snake->m_len-1; i > 0; i--)
    {
        snake->m_body[i].x = snake->m_body[i-1].x;
        snake->m_body[i].y = snake->m_body[i-1].y;
    }
    snake->m_body[0].x = snake->m_coords.x;
    snake->m_body[0].y = snake->m_coords.y;
}

void snake_eat_apple( SNAKE * snake )
{
    if( snake->m_len >= snake->m_resevate )
    {
        printf("resize\n");
        snake->m_resevate *= 2;
        COORDS * new_body = (COORDS*)malloc(snake->m_resevate * sizeof(COORDS));
        for (int i = 0; i < snake->m_len; i++)
            new_body[i] = snake->m_body[i];        
        free(snake->m_body);
        snake->m_body = new_body;
    }
    snake->m_body[snake->m_len] = snake->m_body[snake->m_len-1];
    snake->m_len++;
}

int snake_eat_snake( SNAKE * snake1, SNAKE * snake2 )
{
    for (int i = 0; i < snake2->m_len; i++)
    {
        if( snake1->m_coords.x == snake2->m_body[i].x
            && snake1->m_coords.y == snake2->m_body[i].y )
            return 1;
    }
    return 0;
}

int snake_eat_herself( SNAKE * snake )
{
    for (int i = 3; i < snake->m_len; i++)
    {
        if( snake->m_coords.x == snake->m_body[i].x
            && snake->m_coords.y == snake->m_body[i].y )
            return 1;
    }
    return 0;
}

int snake_eat_wall( SNAKE * snake )
{
    int head_x = snake->m_coords.x;
    int head_y = snake->m_coords.y;
    return map_is_wall( snake->m_map, head_x, head_y );
}