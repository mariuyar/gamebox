#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <termios.h> // для отключения ожидания нажатия enter

#include "color.h"
#include "lcd.h"
#include "utils.h"
#include "font_types.h"
#include "font_prop14x16.c"
// max width = 14
// max height = 16
#include "font.h"
#include "menu.h"
#include "map.h"
#include "game.h"
#include "snake.h"

int init_all( LCD * lcd, FONT * font, MENU * menu );

int main( int argc, char *argv[] ) 
{
    printf("\n\n====================================================\n");
    printf("Welcome!\n");
    printf("Use WASD, enter to control snake or select menu item.\n");
    printf("You can choose one of 2 modes: Single and Demo.\n");
    printf("In Single mode you can control snake by WASD keys.\n");
    printf("Use Demo mode if you want to relax.\n");
    printf("Good luck!\n");
    printf("====================================================\n");

    //printf("Start main\n");

    srand(time(NULL));

    static struct termios oldt, newt;
    tcgetattr( STDIN_FILENO, &oldt );
    newt = oldt;
    // отключить ожидание нажатия enter
    newt.c_lflag &= ~(ICANON);
    // отключить ожидания ввода символа
    newt.c_cc[VMIN] = 0;
    newt.c_cc[VTIME] = 0;
    tcsetattr( STDIN_FILENO, TCSANOW, &newt );
     
    LCD lcd;
    FONT font;
    MENU menu;

    if( init_all( &lcd, &font, &menu ) == 0 )
    {
        printf("Error in initialisation\n");
        return 1;
    }

    led_blink( &lcd, 0x00, 0x00, 0xff, 40, 1 );

    while( 1 )
    {
        if( menu_event( &menu ) )
        {
            if( menu.m_state == START )
            {
                // start game in selected mode
                GAME snake_game;
                game_init( &snake_game, &lcd, &font );
                // добавить 1 змею с ручным управлением
                if( menu.m_mode == MODE1 )
                {
                    Color snake_color = create_color( 255, 255, 255 );
                    if( menu.m_game_color == RED )
                        snake_color = create_color( 255, 0, 0 );
                    else if( menu.m_game_color == GREEN )
                        snake_color = create_color( 0, 255, 0 );
                    else if( menu.m_game_color == BLUE )
                        snake_color = create_color( 0, 0, 255);
                    else
                    {
                        printf("Wrong snake color!\n");
                        game_end( &snake_game );
                        break;
                    }
                    double snake_speed = menu.m_game_speed/10.0; // 0.1, 0.2, 0.3 as the result
                    game_add_snake( &snake_game, MANUAL, snake_color, snake_speed );
                }
                // добавить 2 змеи которые бегают сами
                else if( menu.m_mode == MODE2 )
                {
                    game_add_snake( &snake_game, AUTOPILOT, create_color( 255, 0, 0 ), 0.5 );
                    game_add_snake( &snake_game, AUTOPILOT, create_color( 0, 0, 255 ), 0.5 );
                }
                else
                {
                    printf("Wrong game mode!\n");
                    game_end( &snake_game );
                    break;
                }
                game_start( &snake_game, menu.m_mode );
                game_end( &snake_game );
            }            
        }
        ledline_show( &lcd, 0u );
        lcd_background( &lcd, rgb_to_lcdcolor( 0, 0, 0 ) );
        menu_draw( &menu, &lcd, &font );
        lcd_show( &lcd );
    }
    
    tcsetattr( STDIN_FILENO, TCSANOW, &oldt );
    //printf("End main\n");
    return 0;
}

int init_all( LCD * lcd, FONT * font, MENU * menu )
{
    if( ! lcd_init( lcd ) )
    {
        printf("LCD init error!\n");
        return 0;
    }
    font_init( font, 3 );
    menu_init( menu );
    return 1;
}