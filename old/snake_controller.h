#ifndef __SNAKE_CONTROLLER_H__
#define __SNAKE_CONTROLLER_H__

#include "snake.h"
#include "game.h"

// управление с помощью WASD
void controll_update_direction_manual( GAME * game, SNAKE * snake );
// автономное управление
void controll_update_direction_auto( GAME * game, SNAKE * snake );

#endif//__SNAKE_CONTROLLER_H__