#include "game.h"
#include "map.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
#include "string.h"
#include "termios.h"
#include "time.h"
#include <unistd.h>
#include "math.h"
#include "snake_controller.h"

void controll_update_direction_manual( GAME * game, SNAKE * snake )
{
    COORDS next_direction = snake->m_direction;

    if( snake->m_control == MANUAL )
    {
        char ch = 0;
        int r = read( 0, &ch, 1 );
        if( r != 1 ) return;
        if( ch == 'w' )
        {
            next_direction.x = 0;
            next_direction.y = -1;
        }
        else if( ch == 's' )
        {
            next_direction.x = 0;
            next_direction.y = 1;
        }
        else if( ch == 'a' )
        {
            next_direction.x = -1;
            next_direction.y = 0;
        }
        else if( ch == 'd' )
        {
            next_direction.x = 1;
            next_direction.y = 0;
        }
    }
    snake_update_direction( snake, next_direction );
}

// считает расстояние от точки А до В
double getDist( COORDS a, COORDS b )
{
    int w = abs( a.x - b.x );
    int h = abs( a.y - b.y );
    return sqrt( w*w + h*h );
}

// проверяет является ли В обратным направлением к А
inline int inverseDirection( COORDS a, COORDS b )
{
    return a.x == b.x*-1 && a.y == b.y * -1;
}

inline int equalCoords( COORDS a, COORDS b )
{
    return (a.x == b.x) && (a.y == b.y);
}

int touchSnakes( SNAKE * snake, COORDS coord, GAME * game )
{
    for (int i = 0; i < game->m_snakes_cnt; i++)
        for (int j = 0; j < game->m_snakes[i].m_len; j++)
                if( equalCoords( coord, game->m_snakes[i].m_body[j] ) == 1 ) return 1;
    return 0;
}

inline int touchWalls( SNAKE * snake, COORDS coord, GAME * game )
{
    return map_is_wall( game->m_map, coord.x, coord.y );
}

int canStayHere( SNAKE * snake, COORDS coord, GAME * game )
{
    if( touchSnakes( snake, coord, game ) == 1 ) return 0;
    else if( touchWalls( snake, coord, game ) == 1 ) return 0;
    return 1;
}

void controll_update_direction_auto( GAME * game, SNAKE * snake )
{
    double min_len = 1000000;
    COORDS best_dir = snake->m_direction;

    int x[4] = { -1, 1,  0, 0 };
    int y[4] = {  0, 0, -1, 1 };
    for (int i = 0; i < 4; i++)
    {
        COORDS new_dir;
        new_dir.x = x[i];
        new_dir.y = y[i];
        
        COORDS new_step;
        new_step.x = snake->m_coords.x + new_dir.x;
        new_step.y = snake->m_coords.y + new_dir.y;

        // нельзя развернуться на 180 на месте
        if( inverseDirection( snake->m_direction, new_dir ) == 1 ) continue;

        // проверка если змея может стать на новые координаты
        // там может быть сама змея, другая змея или стена
        if( canStayHere( snake, new_step, game ) == 0 ) continue;

        // если есть маршрут короче то выбираем его
        double len = getDist( game->m_map->apple_coords, new_step );
        if( len < min_len )
        {
            min_len = len;
            best_dir = new_dir;
        }
    }
    snake_update_direction( snake, best_dir );
}
