/*
 Name:		GameBox.ino
 Created:	12.04.2020 15:53:00
 Author:	Yaroslav
*/
#include "src/Connections.h"
#include <LCD5110_Basic.h>
#include <avr/pgmspace.h>

#include "src/Event.h"
#include "src/Menu.h"
#include "src/tetris/TetrisGame.h"
#include "src/snake/snake_menu.h"
#include "src/Settings.h"


extern uint8_t SmallFont[];

LCD5110 * display;
Event * event;
Menu * menu;

// the setup function runs once when you press reset or power the board
void setup() 
{
    //Serial.begin(115200);
    pinMode(LCD_BACKLIGHT, OUTPUT);
    pinMode(SOUND_PIN, OUTPUT);
    Serial.println("start");

    display = new LCD5110(LCD_CLK, LCD_DIN, LCD_DC, LCD_RST, LCD_CE);
    display->InitLCD(); 
    display->setFont(SmallFont);
    display->print("CALIBRATING...", CENTER, 20);
    display->clrScr();
    event = new Event();
    menu = new Menu(display, event);

    Serial.println("init");
}


// the loop function runs over and over again until power down or reset
void loop() 
{
    if ( menu->event() )
    {
        switch (menu->getState())
        {
        case Menu::TETRIS:
        {
            Serial.println("Tetris");
            TetrisGame game(display, event);
            game.start();
            menu->show();
            break;
        }
        case Menu::SNAKE:
        {
            Serial.println("Snake");
            Snake_menu snake_menu(display, event);
            snake_menu.start();
            menu->show();
            break;
        }
        case Menu::SETTINGS:
        {
            Serial.println("Settings");
            Settings settings(display, event);
            settings.start();
            menu->show();
            break;
        }
        default:
            break;
        }
    }
}
