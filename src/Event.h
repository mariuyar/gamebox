#ifndef __EVENT_H__
#define __EVENT_H__

class Joystic;

class Event
{
public:
    Event();
    ~Event();

    enum e_event { J_LEFT_PRESS,
                   J_LEFT_RELEASE, 
                   J_RIGHT_PRESS,
                   J_RIGHT_RELEASE, 
                   J_UP_PRESS,
                   J_UP_RELEASE,
                   J_DOWN_PRESS,
                   J_DOWN_RELEASE,
                   J_PUSH_PRESS,
                   J_PUSH_RELEASE,
                   B_BACK_PRESS,
                   B_BACK_RELEASE,
                   NONE };
    inline int getState() const;
    bool event();
private:
    Joystic* joystic;

    bool left_pressed;
    bool right_pressed;
    bool up_pressed;
    bool down_pressed;
    bool enter_pressed;
    bool back_pressed;
    
    int state;
};

inline int Event::getState() const 
{ 
    return state; 
}

#endif // __EVENT_H__