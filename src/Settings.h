#ifndef __SETTINGS_H__
#define __SETTINGS_H__


class Event;
class LCD5110;

class Settings
{
public:
	Settings(LCD5110* _display, Event* _event);

	enum s_state{ CONTRAST, LED, SOUND };

	void start();

private:
	Event* m_event;
	LCD5110* m_display;
	int m_state;

	void nextState(int _dir);
	void show();
	void setSettings();
};

#endif