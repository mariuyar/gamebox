#include <Arduino.h>
#include "Joystic.h"
#include "Connections.h"

Joystic::Joystic( int urx, int ury, int sw ) 
	: URX_pin(urx), URY_pin(ury), SW_pin(sw)
{
	init();
}

int Joystic::getX()
{
	int x = analogRead(URX_pin);
	return map(x, 0, X_RANGE, X_RANGE/2.0, -1*(X_RANGE/2.0)) + calibrateX;
}

int Joystic::getY()
{
	int y = analogRead(URY_pin);
	return map(y, 0, Y_RANGE, -1*(Y_RANGE / 2.0), (Y_RANGE / 2.0)) + calibrateY;
}

bool Joystic::getSW()
{
	return digitalRead(SW_pin);
}

void Joystic::init()
{
	pinMode(URX_pin, INPUT);
	pinMode(URY_pin, INPUT);
	pinMode(SW_pin, INPUT_PULLUP);
	calibrate();
}

void Joystic::calibrate()
{
	unsigned int ltime = millis();
	unsigned int time = ltime;
	int valx = getX();
	int valy = getY();
	while (time - ltime < 1000)
	{
		int x = getX();
		valx = (valx + x) / 2;
		int y = getY();
		valy = (valy + y) / 2;
		time = millis();
	}
	calibrateX = -1 * valx;
	calibrateY = -1 * valy;
}

