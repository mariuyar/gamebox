#ifndef __SNAKE_CONTROLLER_H__
#define __SNAKE_CONTROLLER_H__

#include "Map.h"
#include "Snake.h"
#include "./../Event.h"

struct Controller
{
	// ���������� � ������� WASD
	static void controll_update_direction_manual(Event* event, Snake* snake);
	// ���������� ����������
	static void controll_update_direction_auto(Snake* snake, Map* map, Snake** snakes, int cnt);
};


#endif//__SNAKE_CONTROLLER_H__
