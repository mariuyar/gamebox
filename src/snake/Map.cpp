#include "Map.h"
#include <LCD5110_Basic.h>
#include "Coords.h"
#include "Arduino.h"

Map::Map(bool walls)
{
    m_height = 24;
    m_width = 42/2;
    m_data = new uint8_t[m_width * m_height];
    // заполнение карты пустотой
    for (int i = 0; i < m_width * m_height; i++)
        m_data[i] = EMPTY;
    if (walls)
        create_walls();
    m_create_apple = false;
}

Map::~Map()
{
    delete[] m_data;
}

uint8_t* Map::getData()
{
    int size = m_width * m_height;
    uint8_t* data = new uint8_t[size];
    for (int i = 0; i < size; i++)
        data[i] = m_data[i];
    return data;
}

void Map::create_walls()
{
    // создание стены сверху снизу
    for (int i = 0; i < m_width; i++)
    {
        m_data[i] = WALL;
        m_data[(m_height-1)*m_width+i] = WALL;
    }
    // создание стены слева справа
    for (int i = 0; i < m_height; i++)
    {
        m_data[i*m_width] = WALL;
        m_data[i*m_width+(m_width-1)] = WALL;
    }
}

void Map::generate_apple()
{
    if (m_create_apple) return;
    m_create_apple = true;
    int x = 1+rand()%(m_width-1);
    int y = 1+rand()%(m_height-1);
    while ( m_data[y*m_width+x] == WALL )
    {
        x = 1+rand()%(m_width-1);
        y = 1+rand()%(m_height-1);
    }
    m_data[y*m_width+x] = APPLE;
    m_apple_coords.m_x = x;
    m_apple_coords.m_y = y;
}

bool Map::is_apple(Coords a)
{
    int pos = a.m_y * m_width + a.m_x;
    return m_data[pos] == APPLE;
}

bool Map::is_wall(Coords a)
{
    int pos = a.m_y * m_width + a.m_x;
    return m_data[pos] == WALL;
}

void Map::remove_apple(Coords a)
{
    int pos = a.m_y * m_width + a.m_x;
    if( m_data[pos] != APPLE )
        return;
    m_create_apple = false;
    m_data[pos] = EMPTY;
    m_data[pos] = 0x00;
}
