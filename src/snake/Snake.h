#ifndef __SNAKE_H__
#define __SNAKE_H__
#include "Map.h"
#include "Coords.h"

enum SNAKE_CONTROL { MANUAL, AUTOPILOT };

class Snake
{
public:
	Snake(int control,
          Map* map,
          double speed,
          int x,
          int y );
	~Snake();

    void update_direction(Coords direction);
    void move();
    // ���� ������������ ������� � ������
    void update_body();
    // �������� ���� �� 1 ���� � ������
    void eat_apple();
    // ��������� ���� ������ ���� ������� ������ ���� 
    bool eat_snake(Snake * other);
    // ��������� ���� ���� ������� ���� ����
    bool eat_herself();
    // ��������� ���� ���� ��������� � �����
    bool eat_wall();
    Coords* getBody()
    {
        return m_body;
    }
    Coords getCoords()
    {
        return m_coords;
    }
    Coords getDirection()
    {
        return m_direction;
    }
    int getLen()
    {
        return m_len;
    }
    int getControl()
    {
        return m_control;
    }
private:
    Map* m_map;
    int m_control; // ��� ����� ����������� 
    double m_speed; // �������� ���� � ������� �� ������� �������
    Coords m_direction;
    int m_len;
    int m_resevate;
    double m_clock;
    Coords m_coords; // ���������� ������ �� �����
    Coords* m_body; // ���������� ����� ���� ������� � ������
    Coords m_last_dir_choose;
};



#endif//__SNAKE_H__