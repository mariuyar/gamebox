#include "snake_menu.h"
#include <LCD5110_Basic.h>
#include "./../Event.h"
#include "./../LCD_utils.h"
#include "Map.h"
#include "SnakeGame.h"

Snake_menu::Snake_menu(LCD5110* display, Event* e)
{
    m_display = display;
    m_e = e;
    m_state = START;
    m_mode = MODE1;
    m_game_speed = SPEED2;
}

void Snake_menu::draw()
{
    m_display->clrScr();

    m_display->print( m_state == START ? "START" : "start", CENTER, 12 );
    
    char* text = (char*)malloc(20 * sizeof(char));
    if (m_mode == MODE1)
        strcpy(text, "< single >\0");
    else
        strcpy(text, "< demo >\0");

    if (m_state == MODE)
        for (int i = 0; i < strlen(text); i++)
            text[i] = toupper(text[i]);
    
    m_display->print( text, CENTER, 24 );
    
    if (m_game_speed == SPEED1)
        strcpy(text, "< low >\0");
    else if (m_game_speed == SPEED2)
        strcpy(text, "< medium >\0");
    else
        strcpy(text, "< fast >\0");

    if (m_state == SPEED)
        for (int i = 0; i < strlen(text); i++)
            text[i] = toupper(text[i]);

    m_display->print(text, CENTER, 36);
    
    free(text);
}

void Snake_menu::start()
{
    bool quit = false;
    while (!quit)
    {
        if (m_e->event())
        {
            switch (m_e->getState())
            {
            case Event::J_PUSH_PRESS:
            {    
                SnakeGame s(m_display, m_e);
                s.add_snake(MANUAL, 0.2);
                s.start();
                break;
            }
            case Event::J_DOWN_PRESS:
                m_state++;
                break;
            case Event::J_UP_PRESS:
                m_state--;
                break;
            case Event::J_LEFT_PRESS:
            {
                if (m_state == MODE)
                    m_mode--;
                else if (m_state == SPEED)
                    m_game_speed--;
                break;
            }
            case Event::J_RIGHT_PRESS:
            {
                if (m_state == MODE)
                    m_mode++;
                else if (m_state == SPEED)
                    m_game_speed++;
                break;
            }
            case Event::B_BACK_PRESS:
                quit = true;
                break;
            default:
                break;
            }
            control_values();
            draw();
        }
    }
}

void Snake_menu::control_state()
{
    if (m_state > SPEED)
        m_state = START;
    else if (m_state < START)
        m_state = SPEED;
}

void Snake_menu::control_mode()
{
    if (m_mode < MODE1)
        m_mode = MODE2;
    else if (m_mode > MODE2)
        m_mode = MODE1;
}

void Snake_menu::control_game_speed()
{
    if (m_game_speed < SPEED1)
        m_game_speed = SPEED3;
    else if (m_game_speed > SPEED3)
        m_game_speed = SPEED1;
}

void Snake_menu::control_values()
{
    control_state();
    control_mode();
    control_game_speed();
}
