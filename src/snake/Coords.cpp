#include "Coords.h"

Coords::Coords()
{
	m_x = 0;
	m_y = 0;
}

Coords::Coords(int x, int y)
{
	m_x = x;
	m_y = y;
}

bool Coords::operator==(const Coords& a)
{
	return a.m_x == m_x && a.m_y == m_y;
}

bool Coords::operator!=(const Coords& a)
{
	return a.m_x != m_x || a.m_y != m_y;
}

Coords operator+(const Coords& a, const Coords & b)
{
	return Coords(b.m_x + a.m_x, b.m_y + a.m_y);
}

Coords operator-(const Coords& a, const Coords& b)
{
	return Coords(b.m_x - a.m_x, b.m_y - a.m_y);
}

Coords operator*(const Coords& a, int n)
{
	return Coords(a.m_x * n, a.m_y * n);
}

Coords operator/(const Coords& a, int n)
{
	return Coords(a.m_x / n, a.m_y / n);
}

void Coords::operator+=(const Coords& a)
{
	m_x += a.m_x;
}

void Coords::operator-=(const Coords& a)
{
	m_x -= a.m_x;
}

void Coords::operator*=(int n)
{
	m_x *= n;
	m_y *= n;
}

void Coords::operator/=(int n)
{
	m_x /= n;
	m_y /= n;
}


