#include "Snake.h"

Snake::Snake(int control, Map* map, double speed, int x, int y)
{
    m_map = map;
    m_control = control;
    m_speed = speed;
    m_speed = speed;
    m_direction = Coords(0, 1); // ���������� ������ ����� �� �����
    m_len = 1;
    m_resevate = 10;
    m_clock = 0;
    m_body = new Coords[m_resevate];
    m_coords = Coords(x, y);  // ������
    m_body[0] = m_coords; // ���� ������� �� 1 �����
}

Snake::~Snake()
{
    delete[] m_body;
}

void Snake::update_direction(Coords direction)
{
    if (m_coords == m_last_dir_choose) return;

    Coords next_direction = direction;

    // ������ ���� � �������� �����������
    if (next_direction * -1 == m_direction)
        next_direction = m_direction;

    if (m_direction != next_direction)
        m_last_dir_choose = m_coords;

    m_direction = next_direction;
}

void Snake::move()
{
    m_clock += m_speed;
    if (m_clock > 1.0)
    {
        m_coords += m_direction;
        m_clock = 0;
    }

    if (m_coords.m_x < 0)
        m_coords.m_x = m_map->getWidth()-1;
    else if (m_coords.m_y < 0)
        m_coords.m_y = m_map->getHeight()-1;
    else if (m_coords.m_x >= m_map->getWidth())
        m_coords.m_x = 0;
    else if (m_coords.m_y >= m_map->getHeight())
        m_coords.m_y = 0;
    update_body();
}

void Snake::update_body()
{
    if (m_coords == m_body[0]) return;
    for (int i = m_len - 1; i > 0; i--)
        m_body[i] = m_body[i - 1];
    m_body[0] = m_coords;
}

void Snake::eat_apple()
{
    if (m_len >= m_resevate)
    {
        m_resevate *= 2;
        Coords* new_body = new Coords[m_resevate];
        if (!new_body) return;
        for (int i = 0; i < m_len; i++)
            new_body[i] = m_body[i];
        delete[] m_body;
        m_body = new_body;
    }
    m_body[m_len] = m_body[m_len - 1];
    m_len++;
}

bool Snake::eat_snake(Snake* other)
{
    Coords* body = other->getBody();
    for (int i = 0; i < other->getLen(); i++)
        if (m_coords == body[i]) return true;
    return false;
}

bool Snake::eat_herself()
{
    for (int i = 1; i < m_len; i++)
        if (m_coords == m_body[i]) return true;
    return false;
}

bool Snake::eat_wall()
{
    return m_map->is_wall(m_coords);
}

