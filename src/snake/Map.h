#ifndef __MAP_H__
#define __MAP_H__
#include <Arduino.h>
#include "Coords.h"

#define NOWALLS 0
#define WITHWALLS 1

enum MAP_OBJECTS{ EMPTY=0, APPLE, WALL };

class Map
{
public:
	Map(bool walls);
	~Map();
    void create_walls();
    int getWidth()
    {
        return m_width;
    }
    int getHeight()
    {
        return m_height;
    }
    // walls - white
    // apple - green
    uint8_t* getData();
    void generate_apple();
    bool is_apple()
    {
        return m_create_apple;
    }
    bool is_apple(Coords a);
    bool is_wall(Coords a);
    void remove_apple(Coords a);
    Coords getAppleCoords()
    {
        return m_apple_coords;
    }

private:
    int m_width;
    int m_height;
    uint8_t* m_data;
    bool m_create_apple;
    Coords m_apple_coords;
};

#endif//__MAP_H__   