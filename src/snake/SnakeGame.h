#ifndef __SNAKE_GAME_H__
#define __SNAKE_GAME_H__

#include "LCD5110_Basic.h"
#include "Map.h"
#include "Snake.h"
#include "./../Event.h"

class SnakeGame
{
public:
    SnakeGame(LCD5110* lcd, Event* event);
	~SnakeGame();

    void start();
    void add_snake(int control, double speed);
    void draw();

private:
    Map * m_map;
    LCD5110* m_lcd;
    Event* m_event;
    int m_snakes_cnt;
    int m_max_snakes_cnt;
    Snake** m_snakes;
    bool m_start_move;
    
    void print_score();
    void update_direction_auto();
    void update_direction_manual();
    void move();
    bool control();
    void eat_apple();
};

#endif // ! __SNAKE_GAME_H__
