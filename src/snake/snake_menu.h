#ifndef __SNAKE_MENU_H__
#define __SNAKE_MENU_H__

#include <LCD5110_Basic.h>
#include "./../Event.h"

enum SNAKE_MENU_STATE{ NONE, START, MODE, SPEED };
enum SNAKE_MENU_SPEED{ SPEED1=1, SPEED2, SPEED3 };
enum SNAKE_MENU_MODE{ MODE1, MODE2 };

class Snake_menu
{
public:
	Snake_menu( LCD5110* display, Event* e );
    void draw();
    void start();

private:
    LCD5110* m_display;
    Event* m_e;

    void control_values();
    void control_state();
    void control_mode();
    void control_game_speed();
    int m_state;
    int m_mode;
    int m_game_speed;
};


#endif//__SNAKE_MENU_H__