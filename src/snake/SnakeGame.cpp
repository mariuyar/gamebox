
#include "SnakeGame.h"
#include "./../LCD_utils.h"
#include "snake_controller.h"
#include "./../Event.h"

SnakeGame::SnakeGame(LCD5110* lcd, Event* event)
{
	m_map = new Map(true);
	m_lcd = lcd;
    m_event = event;
    m_snakes_cnt = 0;
    m_max_snakes_cnt = 5;
    m_snakes = new Snake*[m_max_snakes_cnt];
    m_start_move = false;
}

SnakeGame::~SnakeGame()
{
    for (int i = 0; i < m_snakes_cnt; i++)
        delete m_snakes[i];
    delete[] m_snakes;
	delete m_map;
}

void SnakeGame::add_snake(int control, double speed)
{
    if (m_snakes_cnt > m_max_snakes_cnt) return;
    Serial.println("add new snake");

    int x = 1 + rand() % (m_map->getWidth() - 1);
    int y = 1 + rand() % (m_map->getHeight() - 1);
    Snake * new_snake = new Snake(control, m_map, speed, x, y);
    m_snakes[m_snakes_cnt++] = new_snake;
}

void SnakeGame::start()
{
    int max_snakes_len = m_map->getHeight()*m_map->getWidth();

    draw();
    delay(5000);
   
    bool quit = false;
    while (!quit)
    {
        // ������� ����� ������
        if (m_map->is_apple() == false)
        {
            m_map->generate_apple();
            Serial.println("create apple");
        }
        if (m_event->event())
        {
            switch (m_event->getState())
            {
            case Event::J_UP_PRESS:
                m_start_move = true;
                break;
            case Event::J_DOWN_PRESS:
                m_start_move = true;
                break;
            case Event::J_LEFT_PRESS:
                m_start_move = true;
                break;
            case Event::J_RIGHT_PRESS:
                m_start_move = true;
                break;
            case Event::B_BACK_PRESS:
                quit = true;
                break;
            default:
                break;
            }
        }
        if (m_start_move)
        {
            m_start_move = false;
            update_direction_manual();
        }
        // ���������� ����������� � �������� ����
        update_direction_auto();
        move();
        // �������� �� ������������
        quit = control();
        // �������� �� �������� ������
        eat_apple();
        // ��������� ����� � ����
        //draw();
        delay(100);
    }
    print_score();
    delay(2000);
}

void SnakeGame::draw()
{
    uint8_t* data = m_map->getData();
    for (int i = 0; i < m_snakes_cnt; i++)
    {
        Coords* body = m_snakes[i]->getBody();
        for (int j = 0; j < m_snakes[i]->getLen(); j++)
        {
            int pos = body->m_y * m_map->getWidth() + body->m_x;
            data[pos] = 0x05;
        }
    }
    int left = (42 / 8) - 1;
    int right = 42 / 8 + 42 / 2;
    DisplayUtils::draw_background(left, right);
    DisplayUtils::draw42_24(data, m_map->getWidth(), 9);
	print_score();
    delete data;
}

void SnakeGame::print_score()
{
}

void SnakeGame::update_direction_auto()
{
    for (int i = 0; i < m_snakes_cnt; i++)
    {
        Snake* snake = m_snakes[i];
        if (snake->getControl() == AUTOPILOT)
        {
            Controller::controll_update_direction_auto(snake, m_map, m_snakes, m_snakes_cnt);
            snake->move();
        }
    }
}

void SnakeGame::update_direction_manual()
{
    for (int i = 0; i < m_snakes_cnt; i++)
    {
        Snake* snake = m_snakes[i];
        if (snake->getControl() == MANUAL)
        {
            Controller::controll_update_direction_manual(m_event, snake);
            snake->move();
        }
    }
}

void SnakeGame::move()
{
    for (int i = 0; i < m_snakes_cnt; i++)
    {
        m_snakes[i]->move();
        Serial.print(i);
        Serial.print(":  x: ");
        Serial.print(m_snakes[i]->getCoords().m_x);
        Serial.print("  y: ");
        Serial.println(m_snakes[i]->getCoords().m_y);
    }
}

bool SnakeGame::control()
{
    // �������� �� �������� � �����\������ � ����\� ������ ����
    for (int i = 0; i < m_snakes_cnt; i++)
    {
        if (m_snakes[i]->eat_herself()) return true;
        for (int j = 0; j < m_snakes_cnt; j++)
            if (i != j)
                if (m_snakes[i]->eat_snake(m_snakes[i])) return true;
        if (m_snakes[i]->eat_wall()) return true;
    }
    return false;
}

// ��������� ���� ���-�� ������ ������
void SnakeGame::eat_apple()
{
    for (int i = 0; i < m_snakes_cnt; i++)
    {
        if (m_map->is_apple(m_snakes[i]->getCoords()))
        {
            m_map->remove_apple(m_snakes[i]->getCoords());
            m_snakes[i]->eat_apple();
        }
    }
}
