#include "snake_controller.h"
#include "Map.h"
#include "Coords.h"
#include "math.h"
#include "SnakeGame.h"

void Controller::controll_update_direction_manual(Event* event, Snake* snake)
{
    Coords next_direction = snake->getDirection();
    switch (event->getState())
    {
    case Event::J_UP_PRESS:
        next_direction = Coords(0, -1);
        break;
    case Event::J_DOWN_PRESS:
        next_direction = Coords(0, 1);
        break;
    case Event::J_LEFT_PRESS:
        next_direction = Coords(-1, 0);
        break;
    case Event::J_RIGHT_PRESS:
        next_direction = Coords(1, 0);
        break;
    default:
        break;
    }
    snake->update_direction(next_direction);
}

// ������� ���������� �� ����� � �� �
static double getDist(Coords a, Coords b)
{
    int w = fabs(a.m_x - b.m_x);
    int h = fabs(a.m_y - b.m_y);
    return sqrt(w * w + h * h);
}

// ��������� �������� �� � �������� ������������ � �
static int inverseDirection(Coords a, Coords b)
{
    return a == (b * -1);
}

static bool touchSnakes(Snake* snake, Coords coord, Snake ** snakes, int cnt)
{
    for (int i = 0; i < cnt; i++)
    {
        Coords* body = snakes[i]->getBody();
        for (int j = 0; j < snakes[i]->getLen(); j++)
            if (coord == body[j]) return true;
    }
    return false;
}

static int touchWalls(Snake* snake, Coords coord, Map* map)
{
    return map->is_wall(coord);
}

static bool canStayHere(Snake* snake, Coords coord, Map * map, Snake ** snakes, int cnt)
{
    if (touchSnakes(snake, coord, snakes, cnt)) return false;
    else if (touchWalls(snake, coord, map)) return false;
    return true;
}

void Controller::controll_update_direction_auto(Snake* snake, Map * map, Snake** snakes, int cnt)
{
    double min_len = 1000000;
    Coords best_dir = snake->getDirection();

    int x[4] = { -1, 1,  0, 0 };
    int y[4] = { 0, 0, -1, 1 };
    for (int i = 0; i < 4; i++)
    {
        Coords new_dir(x[i], y[i]);
        Coords new_step = snake->getCoords() + new_dir;

        // ������ ������������ �� 180 �� �����
        if (snake->getDirection() == new_dir*-1) continue;

        // �������� ���� ���� ����� ����� �� ����� ����������
        // ��� ����� ���� ���� ����, ������ ���� ��� �����
        if (canStayHere(snake, new_step, map, snakes, cnt) == 0) continue;

        // ���� ���� ������� ������ �� �������� ���
        double len = getDist(map->getAppleCoords(), new_step);
        if (len < min_len)
        {
            min_len = len;
            best_dir = new_dir;
        }
    }
    snake->update_direction(best_dir);
}
