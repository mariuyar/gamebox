#ifndef __COORDS_H__
#define __COORDS_H__
#include <Arduino.h>
class Coords
{
public:
	Coords();
	Coords( int x, int y );
	bool operator == (const Coords& a);
	bool operator != (const Coords& a);
	friend Coords operator + (const Coords& a, const Coords& b);
	friend Coords operator - (const Coords& a, const Coords& b);
	friend Coords operator * (const Coords& a, int n);
	friend Coords operator / (const Coords& a, int n);
	void operator += (const Coords& a);
	void operator -= (const Coords& a);
	void operator *= (int n);
	void operator /= (int n);

	double m_x, m_y;
};

#endif//__COORDS_H__
