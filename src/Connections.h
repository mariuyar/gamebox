 #ifndef _EVENT_h_
#define _EVENT_h_

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

// ----------- LCD  ---------- //
#define LCD_RST A5
#define LCD_CE  A4
#define LCD_DC  A3
#define LCD_DIN A2
#define LCD_CLK  2
#define LCD_BACKLIGHT  3

static int contrast = 65;
static bool led = true;
static bool sound = true;

// --------- JOYSTICK -------- //
#define J_X  A0
#define J_Y  A1
#define J_SW  8

/*		--------------- treeshold
		v		 v
 -512 ..... 0 ..... 512
*/
// 680 for 3.3V
// 1024 for 5V
#define X_RANGE 680//1024
#define Y_RANGE 680//1024
#define X_TREESHOLD 300//400
#define Y_TREESHOLD 300//400


// --------- BUTTONS --------- //
#define BACKBUTTON_PIN 7

// ---------- SOUND ---------- //
#define SOUND_PIN 9

#endif