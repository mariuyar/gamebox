#ifndef __MENU_H__
#define __MENU_H__

class Event;
class LCD5110;

class Menu
{
public:
	Menu(LCD5110 * _display, Event * _event);

	enum m_event { TETRIS, SNAKE, SETTINGS, NONE };

	inline int getState() const;
	bool event();
	void show();

private:
	Event* m_event;
	LCD5110* m_display;
	int m_state;

	void correctState();
};

inline int Menu::getState() const
{
	return m_state;
}

#endif