#include <Arduino.h>
#include "Connections.h"
#include "Joystic.h"
#include "Event.h"

Event::Event()
{
    joystic = new Joystic(J_X, J_Y, J_SW);
    joystic -> calibrate();
    left_pressed = false;
    right_pressed = false;
    up_pressed = false;
    down_pressed = false;
    enter_pressed = false;
    back_pressed = false;
    state = NONE;
}

Event::~Event()
{
    delete joystic;
}

bool Event::event()
{
    // right
    int x = joystic->getX();
    if (x > X_TREESHOLD)
    {
        if (right_pressed == false)
        {
            right_pressed = true;
            state = J_RIGHT_PRESS;
            return true;
        }
    }
    else
    {
        if (right_pressed == true)
        {
            right_pressed = false;
            state = J_RIGHT_RELEASE;
            return true;
        }
        right_pressed = false;
    }
    // left
    if (x < X_TREESHOLD * -1)
    {
        if (left_pressed == false)
        {
            left_pressed = true;
            state = J_LEFT_PRESS;
            return true;
        }
    }
    else
    {
        if (left_pressed == true)
        {
            left_pressed = false;
            state = J_LEFT_RELEASE;
            return true;
        }
        left_pressed = false;
    }

    // read Y pos
    int y = joystic->getY();
    if (y > Y_TREESHOLD)
    {
        if (down_pressed == false)
        {
            down_pressed = true;
            state = J_DOWN_PRESS;
            return true;
        }
    }
    else
    {
        if (down_pressed == true)
        {
            down_pressed = false;
            state = J_DOWN_RELEASE;
            return true;
        }
        down_pressed = false;
    }
    // left
    if (y < Y_TREESHOLD * -1)
    {
        if (up_pressed == false)
        {
            up_pressed = true;
            state = J_UP_PRESS;
            return true;
        }
    }
    else
    {
        if (up_pressed == true)
        {
            up_pressed = false;
            state = J_UP_RELEASE;
            return true;
        }
        up_pressed = false;
    }

    // read SW button
    if (joystic->getSW())
    {
        if (enter_pressed == false)
        {
            enter_pressed= true;
            state = J_PUSH_PRESS;
            return true;
        }
    }
    else
    {
        if (enter_pressed == true)
        {
            enter_pressed = false;
            state = J_PUSH_RELEASE;
            return true;
        }
        enter_pressed = false;
    }

    // read BACK button
    if (digitalRead(BACKBUTTON_PIN) == HIGH)
    {
        if (back_pressed == false)
        {
            back_pressed = true;
            state = B_BACK_PRESS;
            return true;
        }
    }
    else
    {
        if (back_pressed == true)
        {
            back_pressed = false;
            state = B_BACK_RELEASE;
            return true;
        }
        back_pressed = false;
    }

    state = NONE;
    return false;
}
