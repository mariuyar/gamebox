#ifndef __JOYSTIC_h__
#define __JOYSTIC_h__

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

class Joystic
{
public:
	Joystic(int urx, int ury, int sw);
	bool getSW();
	int getX();
	int getY();

	void calibrate();

private:
	void init();
	

	int URX_pin;
	int URY_pin;
	int SW_pin;

	int calibrateX = 0;
	int calibrateY = 0;
};


#endif

