#include <Arduino.h>
#include "Settings.h"
#include "Event.h"
#include <LCD5110_Basic.h>
#include "Connections.h"

Settings::Settings(LCD5110* _display, Event* _event)
{
	m_display = _display;
	m_event = _event;

	m_state = CONTRAST;
}

void Settings::start()
{
	m_display->clrScr();
	bool quit = false;
	show();

	while ( ! quit )
	{
		if (m_event->event())
		{
			switch ( m_event->getState() )
			{
			case Event::J_UP_PRESS:
			{
				nextState(-1);
				show();
				break;
			}
			case Event::J_DOWN_PRESS:
			{
				nextState(1);
				show();
				break;
			}
			case Event::J_PUSH_PRESS:
			{
				if (m_state == LED)
					led = !led;
				else if (m_state == SOUND)
					sound = !sound;
				setSettings();
				show();
				break;
			}
			case Event::J_LEFT_PRESS:
			{
				if (m_state == CONTRAST)
					if (contrast >= 55)
						contrast -= 5;
				setSettings();
				show();
				break;
			}
			case Event::J_RIGHT_PRESS:
			{
				if (m_state == CONTRAST)
					if (contrast <= 95)
						contrast += 5;
				setSettings();
				show();
				break;
			}
			case Event::B_BACK_PRESS:
			{
				quit = true;
				break;
			}
			default:
				break;
			}
		}
	}

}

void Settings::nextState(int _dir)
{
	m_state += _dir;
	if (m_state < CONTRAST)
		m_state = SOUND;
	if (m_state > SOUND)
		m_state = CONTRAST;
}

void Settings::show()
{
	m_display->clrScr();
	const int initYpos = 15;
	const int initXpos = 10;
	m_display->print((m_state == CONTRAST ? ">Contr: " : " Contr" ), initXpos, initYpos); 
	m_display->printNumI(contrast, initXpos + 40, initYpos, 3);
	
	m_display->print((m_state == LED ? ">Light: " : " Light"), initXpos, initYpos + 8);
	m_display->print((led == true) ? " ON" : " OFF", initXpos + 40, initYpos + 8);

	m_display->print((m_state == SOUND ? ">Sound: " : " Sound"), initXpos, initYpos + 16);
	m_display->print((sound == true) ? " ON" : " OFF", initXpos + 40, initYpos + 16);
}

void Settings::setSettings()
{
	m_display->setContrast(contrast);
	digitalWrite(LCD_BACKLIGHT, (led == true ? LOW : HIGH));
	Serial.println(led == true ? "on" : "off");
}
