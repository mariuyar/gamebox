#include <Arduino.h>
#include "Menu.h"
#include "Event.h"
#include <LCD5110_Basic.h>
#include "Connections.h"


Menu::Menu(LCD5110* _display, Event* _event)
{
	m_display = _display;
	m_event = _event;
	m_state = TETRIS;
	show();
}

bool Menu::event()
{
	if (m_event->event())
	{
		switch ( m_event->getState() )
		{
		case Event::J_UP_PRESS:
			m_state--;
			correctState();
			show();
			break;
		case Event::J_DOWN_PRESS:
			m_state++;
			correctState();
			show();
			break;
		case Event::J_PUSH_PRESS:
			return true;
		default:
			break;
		}
	}
	return false;
}

void Menu::correctState()
{
	if (m_state < TETRIS)
		m_state = SETTINGS;
	else if (m_state > SETTINGS)
		m_state = TETRIS;
}

void Menu::show()
{
	m_display->clrScr();
	switch (m_state)
	{
	case TETRIS:
		m_display->print("TETRIS", CENTER, 20); 
		break;
	case SNAKE:
		m_display->print("SNAKE", CENTER, 20);
		break;
	case SETTINGS:
		m_display->print("SETTINGS", CENTER, 20);
		break;
	default:
		break;
	}
}