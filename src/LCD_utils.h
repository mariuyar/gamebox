#ifndef __DisplayUtils_H__
#define __DisplayUtils_H__

#include "Connections.h"

#define LCD_COMMAND 0 
#define LCD_DATA  1
#define LCD_X     84
#define LCD_Y     48

struct DisplayUtils {

    static void LCDWrite(byte data_or_command, byte data) 
    {
        digitalWrite(LCD_DC, data_or_command); //Tell the LCD that we are writing either to data or a command

        //Send the data
        digitalWrite(LCD_CE, LOW);
        shiftOut(LCD_DIN, LCD_CLK, MSBFIRST, data);
        digitalWrite(LCD_CE, HIGH);
    }

    static void gotoXY(int x, int y) 
    {
        LCDWrite(LCD_COMMAND, 0x80 | x);  // Column.
        LCDWrite(LCD_COMMAND, 0x40 | y);  // Row.  ?
    }

    static void LCDClear(void) 
    {
        for (int index = 0; index < (LCD_X * LCD_Y)/8; index++)
            LCDWrite(LCD_DATA, 0x00);

        gotoXY(0, 0); //After we clear the display, return to the home position
    }
    
    static void draw42_24(uint8_t* data, int width, int shift )
    {
        gotoXY(0, 0);
        for (int i = 0; i < 6; i++)
        {
            gotoXY(shift, i);
            for (int j = 0; j < width; j++)
            {
                unsigned char tmp = 0x00;
                for (int k = i * 4, p = 0; k < i * 4 + 4; k++, p++)
                {
                    if (data[k * width + j] != 0x00)
                        tmp = tmp | (0x03 << 2 * p);
                }
                LCDWrite(LCD_DATA, tmp);
                LCDWrite(LCD_DATA, tmp);
            }
        }
    }

    static void draw_background( int left, int right )
    {
        LCDClear();
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 42; j++)
            {
                if (j < left)
                {
                    LCDWrite(LCD_DATA, 0xff);
                    LCDWrite(LCD_DATA, 0xff);
                }
                else if (j >= left && j < right)
                {
                    LCDWrite(LCD_DATA, 0x00);
                    LCDWrite(LCD_DATA, 0x00);
                }
                else
                {
                    LCDWrite(LCD_DATA, 0xff);
                    LCDWrite(LCD_DATA, 0xff);
                }
            }
        }
    }
};

#endif//__DisplayUtils_H__