#include <Arduino.h>
#include "TetrisGame.h"
#include "./../Event.h"
#include <LCD5110_Basic.h>
#include "./../Connections.h"
#include "Tetris.h"

TetrisGame::TetrisGame(LCD5110* _display, Event* _event)
{
    m_display = _display;
    m_event = _event;

    m_tetris = new Tetris(m_display, m_game_w, m_game_h);
    m_figure = m_tetris->generateFigure(m_game_w / 2, 0);
}

TetrisGame::~TetrisGame()
{
    delete m_tetris;
    delete m_figure;
}

void TetrisGame::start()
{
    int score = 0;
    m_tetris->drawBackground();
    drawScore(score);

    uint32_t ctime = millis();
    int y_cnt = 0;
    bool quit = false;
    int lastchance = 0;
    const int draw_delay_normal = 200;
    int draw_delay = draw_delay_normal;

    bool start_left_cnt = false;
    uint32_t left_move_time;
    bool start_right_cnt = false;
    uint32_t right_move_time;

    while (!quit)
    {
        if (m_event->event())
        {
            switch (m_event->getState())
            {
            case Event::J_LEFT_PRESS:
            {
                left_move_time = millis();
                start_left_cnt = true;
                moveLeft();
                break;
            }
            case Event::J_LEFT_RELEASE:
            {
                start_left_cnt = false;
                break;
            }
            case Event::J_RIGHT_PRESS:
            {
                right_move_time = millis();
                start_right_cnt = true;
                moveRight();
                break;
            }
            case Event::J_RIGHT_RELEASE:
            {
                start_right_cnt = false;
                break;
            }
            case Event::J_DOWN_PRESS:
            {
                draw_delay = 20;
                break;
            }
            case Event::J_UP_PRESS:
            {
                draw_delay = draw_delay_normal;
                break;
            }
            case Event::J_PUSH_PRESS:
            {
                m_figure->rotate();
                if (!m_tetris->canPlaceFigure(m_figure, m_figure->m_x, m_figure->m_y))
                    for (int i = 0; i < 3; i++)
                        m_figure->rotate();
                break;
            }
            case Event::B_BACK_PRESS:
            {
                quit = true;
                break;
            }
            default:
                break;
            }
        }
        
        uint32_t time = millis();
        if (start_left_cnt)
        {
            if (time - left_move_time >= 100)
            {
                left_move_time = time;
                moveLeft();
            }
        }
        if (start_right_cnt)
        {
            if (time - right_move_time >= 100)
            {
                right_move_time = time;
                moveRight();
            }
        }
        if (time - ctime > draw_delay)
        {
            ctime = time;
            m_tetris->placeFigure(m_figure);
            m_tetris->draw();
            m_tetris->getBackFigure(m_figure);

            bool place = false;
            if (!m_tetris->canPlaceFigure(m_figure, m_figure->m_x, m_figure->m_y + 1))
                place = true;
            else
            {
                y_cnt++;
                if (y_cnt == m_y_cnt_max)
                {
                    m_figure->m_y++;
                    y_cnt = 0;
                }
            }

            if (m_figure->m_y + m_figure->m_h >= m_game_h)
                place = true;

            if (place) lastchance++;

            if (place && lastchance >= 3)
            {
                draw_delay = draw_delay_normal;
                lastchance = 0;

                m_tetris->placeFigure(m_figure);
                delete m_figure;
                m_figure = m_tetris->generateFigure(m_game_w / 2, 0);

                // make some noise
                beep();

                // delete full rows
                int res;
                while ((res = m_tetris->controlMap()) != 1 && res != -1)
                    score += 50;

                score++;
                Serial.print("score: ");
                Serial.println(score);
                m_tetris->drawBackground();
                drawScore(score);

                if (res == -1)
                {
                    Serial.println("Game over");
                    drawGameover();
                    quit = true;
                }
            }
        }
    }
}

void TetrisGame::moveLeft()
{
    int newX = m_figure->m_x - 1;
    if (newX >= 0 && m_tetris->canPlaceFigure(m_figure, newX, m_figure->m_y))
        m_figure->m_x = newX;
}

void TetrisGame::moveRight()
{
    int newX = m_figure->m_x + 1;
    if (newX + m_figure->m_w <= m_game_w && m_tetris->canPlaceFigure(m_figure, newX, m_figure->m_y))
        m_figure->m_x = newX;
}

void TetrisGame::drawGameover()
{
    m_display->clrScr();
    bool inv = true;
    for (int i = 0; i < 6; i++)
    {
        m_display->print("GAME OVER", CENTER, 20);
        m_display->invert(inv);
        inv = !inv;
        delay(300);
    }
}

void TetrisGame::drawScore(int _value)
{
    m_display->invertText(true);
    m_display->print("Score:", 5 * 84 / 8, 48 / 3);
    m_display->print(String(_value), 76 - 6 * (int)(log10(_value)), 48 / 2);
    m_display->invertText(false);
}

void TetrisGame::beep()
{
    if (sound)
    {
        digitalWrite(SOUND_PIN, HIGH);
        delay(20);
        digitalWrite(SOUND_PIN, LOW);
    }
}
