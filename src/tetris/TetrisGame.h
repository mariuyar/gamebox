#ifndef __TETRIS_GAME_H__
#define __TETRIS_GAME_H__

class Event;
class LCD5110;
class Tetris;
class Figure;

class TetrisGame
{
public:
	TetrisGame(LCD5110* _display, Event* _event);
	~TetrisGame();
	
	void start();
	
private:
	Tetris* m_tetris;
	Figure* m_figure;

	Event* m_event;
	LCD5110* m_display;
	const uint8_t m_game_h = 24;
	const uint8_t m_game_w = 42/2;
	const int m_y_cnt_max = 1;

	void drawGameover();
	void drawScore(int _value);

	void moveLeft();
	void moveRight();

	void beep();
};

#endif//__TETRIS_GAME_H__