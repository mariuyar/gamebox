#ifndef __TETRIS_H__
#define __TETRIS_H__


class LCD5110;

struct Figure
{
    uint8_t* m_data;
    uint8_t m_w, m_h;
    uint8_t m_x, m_y;
    Figure(const int w, const int h, const int x, const int y, const uint8_t* _data);
    ~Figure();
    void rotate();
};

class Tetris
{
public:
    Tetris(LCD5110 * _display, const uint8_t w, const uint8_t h);
    ~Tetris();

    void draw() const;
    bool canPlaceFigure(const Figure* f, const int x, const int y) const;
    void print() const;
    void drawBackground();

    Figure* generateFigure(int x, int y);
    void placeFigure(const Figure* f);
    void getBackFigure(const Figure* f);

    int controlMap();

    LCD5110 * m_display;
    uint8_t* m_GAMEmap;
private:
    const uint8_t m_w, m_h;
    int* FRA; // array with indexes of full rows
    int FRA_CNT; // counter of full rows

    void cleanRow(int n);
    void shiftDownMap(int n);
};

#endif