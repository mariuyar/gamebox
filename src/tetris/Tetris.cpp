#include <Arduino.h>
#include "Tetris.h"
#include "./../Connections.h"
#include "./../LCD_utils.h"
#include <LCD5110_Basic.h>

/*
static const uint8_t f7_data[] = { 0x00, 0x01, 0x00,   //  #
                                   0x00, 0x01, 0x00,   //  #
                                   0x01, 0x01, 0x01 }; // ###
*/
/*
* ##     #   ##  #
* #     ##    #  ##
* #     #     #   #
*
* ####   ##      #
*        ##     ###
*/
static const uint8_t f1_data[] = { 0x00, 0x01, 0x00,
                                   0x01, 0x01, 0x01 };

static const uint8_t f2_data[] = { 0x01, 0x01,
                                   0x01, 0x01 };

static const uint8_t f3_data[] = { 0x01, 0x01, 0x01,
                                   0x01, 0x00, 0x00 };

static const uint8_t f4_data[] = { 0x01, 0x00,
                                   0x01, 0x01,
                                   0x00, 0x01 };

static const uint8_t f5_data[] = { 0x01, 0x01, 0x01, 0x01 };

static const uint8_t f6_data[] = { 0x01, 0x01, 0x01,
                                   0x00, 0x00, 0x01 };

static const uint8_t f7_data[] = { 0x00, 0x01,
                                   0x01, 0x01,
                                   0x01, 0x00 };


Figure::Figure(const int w, const int h, const int x, const int y, const uint8_t* _data)
{
    m_w = w;
    m_h = h;
    m_x = x;
    m_y = y;
    m_data = new uint8_t[m_w * m_h];
    for (int i = 0; i < m_w * m_h; i++)
        m_data[i] = _data[i];
}

Figure::~Figure()
{
    delete m_data;
}


void Figure::rotate()
{
    uint8_t* newData = new uint8_t[m_w * m_h];
    for (int i = 0, y = 0; i < m_h; i++, y++)
    {
        for (int j = 0, x = m_w - 1; j < m_w; j++, x--)
        {
            int pos1 = i * m_w + j;
            int pos2 = x * m_h + y;
            newData[pos2] = m_data[pos1];
        }
    }
    int tmp = m_w;
    m_w = m_h;
    m_h = tmp;
    delete m_data;
    m_data = newData;
}


// -------- TETRIS ---------- //

Tetris::Tetris(LCD5110* _display, const uint8_t w, const uint8_t h) : m_w(w), m_h(h)
{
    m_GAMEmap = new uint8_t[m_w * m_h];

    for (int i = 0; i < m_w * m_h; i++)
        m_GAMEmap[i] = 0;

    FRA_CNT = 0;
    FRA = new int[m_h];
}

Tetris::~Tetris()
{
    delete[] m_GAMEmap;
    delete[] FRA;
}

void Tetris::drawBackground()
{
    int left = 42 / 8;
    int right = 42 / 8 + 42 / 2 - 1;
    DisplayUtils::draw_background(left, right);
}

void Tetris::draw() const
{
    DisplayUtils::draw42_24(m_GAMEmap, m_w, 9);
    /*
    for (int i = 0; i < 6; i++)
    {
        DisplayUtils::gotoXY(84 / 8 - 1, i);
        for (int j = 0; j < m_w; j++)
        {
            unsigned char tmp = 0x00;
            for (int k = i * 4, p = 0; k < i * 4 + 4; k++, p++)
            {
                if (m_GAMEmap[k * m_w + j] != 0x00)
                    tmp = tmp | (0x03 << 2 * p);
            }
            DisplayUtils::LCDWrite(LCD_DATA, tmp);
            DisplayUtils::LCDWrite(LCD_DATA, tmp);
        }
    }
    */
}

bool Tetris::canPlaceFigure(const Figure* f, const int x, const int y) const
{
    if (x < 0 || y + f->m_h > m_h) return false;

    uint8_t* data = f->m_data;
    for (size_t i = 0; i < f->m_h; i++)
    {
        for (size_t j = 0; j < f->m_w; j++)
        {
            if (data[i * f->m_w + j] == 0x01 &&
                m_GAMEmap[(y + i) * m_w + (x + j)] == 0x01) return false;
        }
    }
    return true;
}

void Tetris::placeFigure(const Figure* f)
{
    for (int i = 0; i < m_h; i++)
    {
        for (int j = 0; j < m_w; j++)
        {
            if (i >= f->m_y && i < f->m_y + f->m_h && j >= f->m_x && j < f->m_x + f->m_w)
            {
                if (f->m_data[(i - f->m_y) * f->m_w + (j - f->m_x)] == 0x01)
                {
                    m_GAMEmap[i * m_w + j] = 0x01;
                }
            }
        }
    }
    // end
}

void Tetris::getBackFigure(const Figure* f)
{
    for (int i = 0; i < m_h; i++)
    {
        for (int j = 0; j < m_w; j++)
        {
            if (i >= f->m_y && i < f->m_y + f->m_h && j >= f->m_x && j < f->m_x + f->m_w)
            {
                if (f->m_data[(i - f->m_y) * f->m_w + (j - f->m_x)] == 0x01)
                {
                    m_GAMEmap[i * m_w + j] = 0x00;
                }
            }
        }
    }
    // end
}

Figure* Tetris::generateFigure(int x, int y)
{
    Figure* f = nullptr;

    int r = rand() % 7;
    if (r == 0)
        f = new Figure(3, 2, x, y, f1_data);
    else if (r == 1)
        f = new Figure(2, 2, x, y, f2_data);
    else if (r == 2)
        f = new Figure(3, 2, x, y, f3_data);
    else if (r == 3)
        f = new Figure(2, 3, x, y, f4_data);
    else if (r == 4)
        f = new Figure(4, 1, x, y, f5_data);
    else if (r == 5)
        f = new Figure(3, 2, x, y, f6_data);
    else
        f = new Figure(2, 3, x, y, f7_data);
    return f;
}

void Tetris::print() const
{
    for (int i = 0; i < m_h; i++)
    {
        for (int j = 0; j < m_w; j++)
        {
            Serial.print(m_GAMEmap[i * m_w + j]);
            Serial.print(" ");
        }
        Serial.println();
    }
    Serial.println();
}

void Tetris::cleanRow(int n)
{
    for (int i = 0; i < m_w; i++)
    {
        m_GAMEmap[n * m_w + i] == 0x00;
    }
}

void Tetris::shiftDownMap(int n)
{
    for (size_t i = n; i > 0; i--)
    {
        for (size_t j = 0; j < m_w; j++)
        {
            m_GAMEmap[i * m_w + j] = m_GAMEmap[(i - 1) * m_w + j];
        }
    }
}

int Tetris::controlMap()
{
    if (FRA_CNT != 0)
    {
        cleanRow(FRA[--FRA_CNT]);
        shiftDownMap(FRA[FRA_CNT]);
        return 0;
    }

    for (int i = m_h - 1; i >= 0; i--)
    {
        int cnt = 0;
        for (int j = 0; j < m_w; j++)
        {
            if (m_GAMEmap[i * m_w + j] != 0x00)
                cnt++;
        }
        if (i == 0 && cnt > 0)
        {
            return -1;
        }
        if (cnt == m_w)
        {
            FRA[FRA_CNT] = i;
            FRA_CNT++;
        }
    }
    if (FRA_CNT == 0)
        return 1;
    else
    {
        cleanRow(FRA[--FRA_CNT]);
        shiftDownMap(FRA[FRA_CNT]);
        return 0;
    }
}
