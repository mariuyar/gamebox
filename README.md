﻿GAME BOX 

Installation & setup manual 

The subject of the coursework is development of the game box. I used Arduino Uno and other components(v. below) to realise this project. Program is written in C. 

Connected files: 

- Connections.h 
- Event.h\cpp 
- Joystic.h\cpp 
- Menu.h\cpp 
- Settings.h\cpp 
- Tetris.h\cpp 
- TetrisGame.h\cpp 

External libraries: 

- LCD5110\_Basic 
1. States of device: 

Device can be in one of the basic states: 

- Displaying  menu and waiting  for user to choose one of the menu items 
- Playing Tetris 
- Settings 

It is enough to add new state to Menu and launch it in the main cycle **to add new game.** 

if ( menu->event() ) { 

`       `switch (menu->getState()) 

Case 1: run tetris 

Case 2: run … 

… 

Case n: run settings 

![](img/README.001.png)

Graph representating switching between the main game states. 

2. Description of the main project stages. 
1. Menu 

Menu can have 3 states: TETRIS, SETTINGS, NONE.   

![](img/README.002.png)

Graph of Menu switching 

2. Settings 

Setting can have 3 states: CONTRAST, LED, SOUND. It is possible to change contrast, turn on/off screen backlight and turn the sound on/off in the Setting. 

![](img/README.003.png)

Graph of Settings switching 

3. Game Tetris 

Game contains 4 files: Tetris.h\cpp TetrisGame.h\cpp. Tetris.h\cpp is the  game  kernel,  contains  class  Tetris  and  class  Figure,  whereas TetrisGame.h\cpp manages game and processes events. Game resolution is the half of resolution of the screen 42\*24. 

![](img/README.004.png)

Graph of game switching 

Classical Tetris figures are used in the game. Drawing of already displayed figures (stored in gamemap) and the falling one happens separately. As soon 

the figure is on the lower bound of the map it is copied into gamemap and new figure appears it the central part of the upper bound. 

![](img/README.005.png)

Tetris figures 

Rotation algorithm(90  degrees): 

for ( i = 0, y = 0; i < h; i++, y++) 

`       `for ( j = 0, x = w - 1; j < w; j++, x--) 

`                   `newData[i][j] = m\_data[x][y]; 



||0 |1 |2 |3 ||<p>![](img/README.006.png)</p><p>Rotation(90 deg left)</p>|
| :- | - | - | - | - | :- | - |
||4 |5 |6 |7 |||
||8 |9 |a |b |||
||c |d |e |f |||
||3 |7 |b |f |||
||2 |6 |a |e |||
||1 |5 |9 |d |||
||0 |4 |8 |c |||
3. Display features 

Display module is a matrix of LC elements and a microcircuit PCD8544 to control them. It is placed in a corpus on the board. There are also four light emmiting  diodes  on  the  board,  which  are  used  for  a  screen  backlight. Information  about  the  state  of  display  pixels  is  stored  in  the  RAM  of  the microcircuit  PCD8544.  Display  resolution  is  84\*48  pixels.  Every  pixel corresponds to one bit of memory.   

![](img/README.007.png)

Addressing of the indicator for every pixel.  Overall we have 84х48  pixels, organized into 6 vertical 

cells (from 0 to 5) and 84 columns.

Because  pixels  are  vertically  grouped  for  8  pixels  into  1  cell,  it  is impossible to directly manage every pixel separately. It makes drawing game on screen more complicated. Function Tetris::draw() was written for this purpose, it  complments  library  LCD5110\_Basic. 

4. Connection of device components 

Ports of connected modules can be included in file Connections.h LIST OF NECESSARY COMPONENTS 

- 1x Arduino Uno 
- 1x PS2 X Y Joystick module 
- 1x Graphic LCD 84-48 – Nokia 5110 
- 1x Breadboard 
- Jump wires 
- 2x 10 kOhm resistor 
- 1x npn transistor 
- 1x Buzzer 

![](img/README.008.png)

The scheme of connection 

5. Video [https://youtu.be/_sqX-oog694 ](https://youtu.be/_sqX-oog694)Old[: https://youtu.be/IfRjmeZvoTU ](https://youtu.be/IfRjmeZvoTU)
6. Resourses: 

https://www.arduino.cc/ [https://www.sparkfun.com/datasheets/LCD/Monochrome/Nokia5110.pdf ](https://www.sparkfun.com/datasheets/LCD/Monochrome/Nokia5110.pdf)https://www.diagrameditor.com/ 
